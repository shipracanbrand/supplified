<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function index() {
        //return "hi";
    }

    public function checkAuthentication($params) {

        try {
            $this->load->library('form_validation');
            $this->load->config('custom-config');
            if ($this->form_validation->required($params['username']) === FALSE) {
                $arr['status'] = 0;
                $arr['message'] = 'username is invalid';
                $arr['should_be'] = 'As Required';
                return $arr;
            } elseif ($this->form_validation->required($params['password']) === FALSE) {
                $arr['status'] = 0;
                $arr['message'] = 'password is invalid';
                $arr['should_be'] = 'As Required';
                return $arr;
            } else {
                if ($params['username'] == $this->config->item('USERNAME') && $params['password'] == $this->config->item('PASSWORD')) {
                    $arr['status'] = 1;
                    $arr['message'] = 'Data Valided';
                    return $arr;
                } else {
                    $arr['status'] = 0;
                    $arr['message'] = 'Invalid username or password';
                    return $arr;
                }
            }
        } catch (Exception $ex) {
            $result['status'] = "Failed";
            $result['errors'] = $ex->getMessage();
            return $result;
        }
    }

    public function addreview() {

        $value = array();
        if (isset($_REQUEST)) {
            $value = $_REQUEST;
        }
        $result = $this->checkAuthentication($value);
        if ($result['status'] == 1) {
            $this->load->library('review');
            $result = $this->review->addReview($value);
            $json_data = json_encode($result);
            $data['response'] = $json_data;
        } else {
            $json_data = json_encode($result);
            $data['response'] = $json_data;
        }
        $this->load->view('responseData', $data);
    }
    
    public function addOrder() {
        $value = array();
        if (isset($_REQUEST)) {
            $value = $_REQUEST;
        }
        $result = $this->checkAuthentication($value);
        if ($result['status'] == 1) {
            $this->load->library('order');
            $result = $this->order->addOrder($value);
            $json_data = json_encode($result);
            $data['response'] = $json_data;
        } else {
            $json_data = json_encode($result);
            $data['response'] = $json_data;
        }
        $this->load->view('responseData', $data);
    }

}
