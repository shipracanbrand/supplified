<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class validation {

    public function validate_review($params) {

        try {
            $CI = & get_instance();
            $CI->load->library('form_validation');
            if ($CI->form_validation->required($params['name']) == False) {
                $result['status'] = 0;
                $result['msg'] = "Fail to save data";
                $result['errors'][] = "Name Required";
            }
            if ($CI->form_validation->min_length($params['name'], 4) == False) {
                $result['status'] = 0;
                $result['msg'] = "Fail to save data";
                $result['errors'][] = "Name Min length 4 charaters";
            }
            if ($CI->form_validation->max_length($params['name'], 100) == False) {
                $result['status'] = 0;
                $result['msg'] = "Fail to save data";
                $result['errors'][] = "Name Max length 100 charaters";
            }
            if ($CI->form_validation->required($params['title_of_review']) == False) {
                $result['status'] = 0;
                $result['msg'] = "Fail to save data";
                $result['errors'][] = "Tilte of review required";
            }
            if ($CI->form_validation->min_length($params['title_of_review'], 4) == False) {
                $result['status'] = 0;
                $result['msg'] = "Fail to save data";
                $result['errors'][] = "Tilte of review Min length 4 charaters";
            }
            if ($CI->form_validation->required($params['title_of_review']) == False) {
                $result['status'] = 0;
                $result['msg'] = "Fail to save data";
                $result['errors'][] = "Tilte of review required";
            }
            if ($CI->form_validation->min_length($params['review'], 4) == False) {
                $result['status'] = 0;
                $result['msg'] = "Fail to save data";
                $result['errors'][] = "Review Min length 4 charaters";
            }
            if ($CI->form_validation->required($params['ip_address']) == False) {
                $result['status'] = 0;
                $result['msg'] = "Fail to save data";
                $result['errors'][] = "IP Address required";
            }

            if (empty($result)) {
                $result['status'] = 1;
                $result['msg'] = "valid data";
            }
            return $result;
        } catch (Exception $ex) {
            $result['status'] = "0";
            $result['msg'] = "Fail to save data";
            $result['errors'] = $ex->getMessage();
            return $result;
        }
    }

    public function validate_order($params) {
        try {
            $CI = & get_instance();
            $CI->load->library('form_validation');
//            if ($CI->form_validation->required($params['user_name']) == False) {
//                $result['errors'][] = "User Name Required";
//            }
//            if ($CI->form_validation->min_length($params['user_name'], 4) == False) {
//                $result['errors'][] = "User Name Min length 4 charaters";
//            }
//            if ($CI->form_validation->max_length($params['user_name'], 100) == False) {
//                $result['errors'][] = "User Name Max length 100 charaters";
//            }
//            if ($CI->form_validation->required($params['user_email']) == False) {
//                $result['errors'][] = "User Email required";
//            }
//            if ($CI->form_validation->valid_email($params['user_email']) == False) {
//                $result['errors'][] = "Invalid Email Address";
//            }
            if ($CI->form_validation->required($params['source_url']) == False) {
                $result['errors'][] = "Source url required";
            }
            if ($CI->form_validation->required($params['billing_name']) == False) {
                $result['errors'][] = "Billing Name required";
            }
            if ($CI->form_validation->min_length($params['billing_name'], 4) == False) {
                $result['errors'][] = "Billing Name length 4 charaters";
            }
            if ($CI->form_validation->required($params['billing_email']) == False) {
                $result['errors'][] = "Billing Email required";
            }
            if ($CI->form_validation->valid_email($params['billing_email']) == False) {
                $result['errors'][] = "Invalid Billing Email Address";
            }
            if ($CI->form_validation->required($params['billing_address']) == False) {
                $result['errors'][] = "Billing Address required";
            }
            if ($CI->form_validation->required($params['billing_city_id']) == False) {
                $result['errors'][] = "Billing City required";
            }
            if ($CI->form_validation->required($params['billing_state_id']) == False) {
                $result['errors'][] = "Billing State required";
            }
            if ($CI->form_validation->required($params['billing_pincode']) == False) {
                $result['errors'][] = "Billing Pincode required";
            }
            if ($CI->form_validation->numeric($params['billing_pincode']) === FALSE) {
                $result['errors'][] = "Billing Pincode should be numeric number";
            }
            if ($CI->form_validation->exact_length($params['billing_pincode'], 6) === FALSE) {
                $result['errors'][] = "Billing Pincode should be a 6 digit numeric number";
            }
            if ($CI->form_validation->required($params['shipping_name']) == False) {
                $result['errors'][] = "Shipping Name required";
            }
            if ($CI->form_validation->min_length($params['shipping_name'], 4) == False) {
                $result['errors'][] = "Shipping Name length 4 charaters";
            }
            if ($CI->form_validation->required($params['shipping_phone']) == False) {
                $result['errors'][] = "Shipping Phone no required";
            }
            if ($CI->form_validation->numeric($params['shipping_phone']) === FALSE) {
                $result['errors'][] = "Shipping Phone should be numeric number";
            }
            if ($CI->form_validation->exact_length($params['shipping_phone'], 10) === FALSE) {
                $result['errors'][] = "Shipping Phone should be a 10 digit numeric number";
            }
            if ($CI->form_validation->required($params['shipping_address']) == False) {
                $result['errors'][] = "Shipping Address required";
            }
            if ($CI->form_validation->required($params['shipping_city_id']) == False) {
                $result['errors'][] = "Shipping City required";
            }
            if ($CI->form_validation->required($params['shipping_state_id']) == False) {
                $result['errors'][] = "Shipping State required";
            }
            if ($CI->form_validation->required($params['shipping_pincode']) == False) {
                $result['errors'][] = "Shipping Pincode required";
            }
            if ($CI->form_validation->numeric($params['shipping_pincode']) === FALSE) {
                $result['errors'][] = "Shipping Pincode should be numeric number";
            }
            if ($CI->form_validation->exact_length($params['shipping_pincode'], 6) === FALSE) {
                $result['errors'][] = "Shipping Pincode should be a 6 digit numeric number";
            }
            if ($CI->form_validation->required($params['order_prefix']) == False) {
                $result['errors'][] = "Order Prefix required";
            }
            if ($CI->form_validation->required($params['order_source']) == False) {
                $result['errors'][] = "Order Source required";
            }
            if ($CI->form_validation->required($params['source_type']) == False) {
                $result['errors'][] = "Source Type required";
            }
            if ($CI->form_validation->required($params['source_id']) == False) {
                $result['errors'][] = "Source ID required";
            }
            if ($CI->form_validation->numeric($params['source_id']) === FALSE) {
                $result['errors'][] = "Source ID should be numeric number";
            }
            if ($CI->form_validation->required($params['source_name']) == False) {
                $result['errors'][] = "Source Name required";
            }
            if ($CI->form_validation->required($params['payment_method']) == False) {
                $result['errors'][] = "Payment Method required";
            }
            if ($CI->form_validation->required($params['total_discount']) == False) {
                $result['errors'][] = "Total Discount required";
            }
            if ($CI->form_validation->numeric($params['total_discount']) === FALSE) {
                $result['errors'][] = "Total Discount should be numeric number";
            }
            if ($CI->form_validation->required($params['total_payable_amount']) == False) {
                $result['errors'][] = "Total Payable Amount required";
            }
            if ($CI->form_validation->numeric($params['total_payable_amount']) === FALSE) {
                $result['errors'][] = "Total Payable Amount should be numeric number";
            }
            if ($CI->form_validation->required($params['total_paid_amount']) == False) {
                $result['errors'][] = "Total Paid Amount required";
            }
            if ($CI->form_validation->numeric($params['total_paid_amount']) === FALSE) {
                $result['errors'][] = "Total Paid Amount should be numeric number";
            }
            if ($CI->form_validation->required($params['sub_total']) == False) {
                $result['errors'][] = "Sub Total required";
            }
            if ($CI->form_validation->numeric($params['sub_total']) === FALSE) {
                $result['errors'][] = "Sub Total should be numeric number";
            }
            if ($CI->form_validation->required($params['discounted_price_total']) == False) {
                $result['errors'][] = "Discounted Price Total required";
            }
            if ($CI->form_validation->numeric($params['discounted_price_total']) === FALSE) {
                $result['errors'][] = "Discounted Price Total should be numeric number";
            }
            if ($CI->form_validation->required($params['order_type']) === FALSE) {
                $result['errors'][] = "Order Type required";
            }
            if ($CI->form_validation->required($params['coupon_code']) === FALSE) {
                $result['errors'][] = "Coupon Code required";
            }

            $product_arr = array();
            $count = count($params['product_details']);
            if ($count > 0) {
                for ($i = 0; $i < $count; $i++) {

                    if ($CI->form_validation->required($params['product_details'][$i]['subscribed_product_id']) == False) {
                        $product_arr['live']['errors'][] = "Subscribed Product ID required";
                    }
                    if ($CI->form_validation->numeric($params['product_details'][$i]['subscribed_product_id']) === FALSE) {
                        $product_arr['live']['errors'][] = "Subscribed Product ID should be numeric number";
                    }
                    if ($CI->form_validation->required($params['product_details'][$i]['base_product_id']) == False) {
                        $product_arr['live']['errors'][] = "Base Product ID required";
                    }
                    if ($CI->form_validation->numeric($params['product_details'][$i]['base_product_id']) === FALSE) {
                        $product_arr['live']['errors'][] = "Base Product ID should be numeric number";
                    }
                    if ($CI->form_validation->required($params['product_details'][$i]['store_id']) == False) {
                        $product_arr['live']['errors'][] = "Store ID required";
                    }
                    if ($CI->form_validation->numeric($params['product_details'][$i]['store_id']) === FALSE) {
                        $product_arr['live']['errors'][] = "Store ID should be numeric number";
                    }
                    if ($CI->form_validation->required($params['product_details'][$i]['store_offer_price']) == False) {
                        $product_arr['live']['errors'][] = "Store Offer Price required";
                    }
                    if ($CI->form_validation->numeric($params['product_details'][$i]['store_offer_price']) === FALSE) {
                        $product_arr['live']['errors'][] = "Store Offer Price should be numeric number";
                    }
                    if ($CI->form_validation->required($params['product_details'][$i]['product_name']) == False) {
                        $product_arr['live']['errors'][] = "Product Name Price required";
                    }
                    if ($CI->form_validation->required($params['product_details'][$i]['discount']['type']) == False) {
                        $product_arr['live']['errors'][] = "Discount Type required";
                    }
                    if ($CI->form_validation->required($params['product_details'][$i]['discount']['value']) == False) {
                        $product_arr['live']['errors'][] = "Discount Value required";
                    }
                    if ($CI->form_validation->required($params['product_details'][$i]['discount']['amount']) == False) {
                        $product_arr['live']['errors'][] = "Discount Amount required";
                    }
                    if ($CI->form_validation->required($params['product_details'][$i]['discounted_price']) == False) {
                        $product_arr['live']['errors'][] = "Discounted Price required";
                    }
                    if ($CI->form_validation->required($params['product_details'][$i]['product_qty']) == False) {
                        $product_arr['live']['errors'][] = "Product Quantity Price required";
                    }
                }
                if (!empty($product_arr)) {
                    $result['errors'][] = $product_arr;
                }
            }
            if (empty($result)) {
                $result['status'] = 1;
                $result['msg'] = "valid data";
            } else {
                $result['status'] = 0;
                $result['msg'] = "Fail to save data";
                $result['errors'] = $result['errors'];
            }
            return $result;
        } catch (Exception $ex) {
            $result['status'] = "0";
            $result['msg'] = "Fail to save data";
            $result['errors'] = $ex->getMessage();
            return $result;
        }
    }

}
