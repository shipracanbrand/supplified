<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class order {

    public function addOrder($params) {

        try {
            $CI = & get_instance();
            $CI->load->model('Product_model');
            $CI->load->library('validation');
            $params = $params['data'];
            $products = $params['product_details'];
            $result['status'] = 1;
            //$result = $CI->validation->validate_order($params);
            if ($result['status'] == 1) {
                $count = count($products);
                for ($i = 0; $i < $count; $i++) {
                    $cols = array('subscribed_product_id');
                    if (!empty($products[$i]['subscribed_product_id'])) {
                        $cond['subscribed_product_id'] = $products[$i]['subscribed_product_id'];
                    } elseif (!empty($products[$i]['base_product_id']) && !empty($products[$i]['store_id'])) {
                        $cond['base_product_id'] = $products[$i]['base_product_id'];
                        $cond['store_id'] = $products[$i]['store_id'];
                    } else {
                        $result['status'] = "Failed";
                        $result['msg'] = "Save to fail data";
                        $result['errors'] = "Invalid  subscribed product id,base_product_id or store_id";
                        return $result;
                    }
                    $Res_arr = $CI->Product_model->getProductData($cond, $cols);
                    if(empty($Res_arr)){
                        $result['status'] = "Failed";
                        $result['msg'] = "Save to fail data";
                        $result['errors'] = "Invalid  subscribed product id,base_product_id or store_id";
                        return $result;
                    }
                    print_r($Res_arr); die;
                }
                $data = array();
                $CI->load->model('Order_model');
                //$data['user_name'] = $params['user_name'];
                // $data['user_email'] = $params['user_email'];
                $data['source_url'] = $params['source_url'];
                $data['billing_name'] = $params['billing_name'];
                $data['billing_email'] = $params['billing_email'];
                // $data['billing_phone'] = $params['billing_phone'];
                $data['billing_address'] = $params['billing_address'];
                $data['billing_city_id'] = $params['billing_city_id'];
                $data['billing_state_id'] = $params['billing_state_id'];
                $data['billing_pincode'] = $params['billing_pincode'];
                $data['shipping_name'] = $params['shipping_name'];
                //$data['shipping_email'] = $params['shipping_email'];
                $data['shipping_phone'] = $params['shipping_phone'];
                $data['shipping_address'] = $params['shipping_address'];
                $data['shipping_state_id'] = $params['shipping_state_id'];
                $data['shipping_city_id'] = $params['shipping_city_id'];
                $data['shipping_pincode'] = $params['shipping_pincode'];
                $data['order_prefix'] = $params['order_prefix'];
                $data['order_source'] = $params['order_source'];
                $data['source_type'] = $params['source_type'];
                $data['source_id'] = $params['source_id'];
                $data['source_name'] = $params['source_name'];
                $data['payment_mod'] = $params['payment_method'];
                $data['total_discount'] = $params['total_discount'];
                $data['total_payable_amount'] = $params['total_payable_amount'];
                $data['sub_total'] = $params['sub_total'];
                $data['discounted_price_total'] = $params['discounted_price_total'];
                $data['order_type'] = $params['order_type'];
                $data['total_paid_amount'] = $params['total_paid_amount'];
                $data['coupon_code'] = $params['coupon_code'];
                $id = $CI->Order_model->saveOrderData($data);
                if (!empty($id)) {
                    $count = count($products);
                    for ($i = 0; $i < $count; $i++) {
                        $pdata['order_id'] = $id;
                        $pdata['subscribed_product_id'] = $products[$i]['subscribed_product_id'];
                        $pdata['base_product_id'] = $products[$i]['base_product_id'];
                        $pdata['store_id'] = $products[$i]['store_id'];
                        $pdata['discount'] = $products[$i]['discount']['amount'];
                        $pdata['price'] = $products[$i]['store_offer_price'];
                        $pdata['discounted_price'] = $products[$i]['discounted_price'];
                        //$products[$i]['discount']['amount'];
                        //['discount']['value'];
                        $pdata['qty'] = $products[$i]['product_qty'];
                        //$pdata['total_price'] = $products[$i]['total_paid_amount'];
                        $pdata['product_name'] = $products[$i]['product_name'];
                        $pdata['discount_type'] = $products[$i]['discount']['type'];
                        //$pdata['discount_qty'] = $products[$i]['discount_qty'];
                        $pid = $CI->Order_model->saveOrderProductData($pdata);
                        if ($pid) {
                            $result['status'] = "Success";
                            $result['msg'] = "Data save successfully";
                        } else {
                            $result['status'] = "Failed";
                            $result['msg'] = "Save to fail data in order line";
                        }
                    }
                } else {
                    $result['status'] = "Failed";
                    $result['msg'] = "Save to fail data in order header";
                }
            } else {
                $result['status'] = "Failed";
            }
            return $result;
        } catch (Exception $ex) {
            $result['status'] = "Fail";
            $result['errors'] = $ex->getMessage();
            return $result;
        }
    }

}
