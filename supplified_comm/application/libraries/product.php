<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class product {

    public function productList($params) { //Used in the booking Engine
        try {
            $CI = & get_instance();
            $CI->load->config('custom-config');
            $url = $CI->config->item('SOLR_URL');
            
            $query_condition = "*:*";
            $parameters = "*";
            if (!empty($params['filter'])) {
                $filter = $params['filter'];
                $query_condition_res = $this->searching($filter);
                if($query_condition_res['status']==0){
                     return $query_condition_res;
                } else {
                    $query_condition=$query_condition_res['query_condition'];
                }
            }
            if (!empty($params['min']) && $params['min'] = 1) {
                $parameters = $CI->config->item('SOLR_MIN_PARAM');
            } else if (!empty($params['micro']) && $params['micro'] = 1) {
                $parameters = $CI->config->item('SOLR_MICRO_PARAM');
            }

            $facets_fields=$this->facets();
            $sort_fileds='';
             if (!empty($params['sort'])) {
                $sort_fileds=$this->sorting($params['sort']);
             }
            //Complete Solr Url With Parameter
            $url = $url . "select?q=" . urlencode($query_condition) . "&fl=" . $parameters . "&sort=".urlencode($sort_fileds)."&facet=true".$facets_fields."&wt=json&indent=true";
            
          // print_r($url); die;
            $product_list = $this->httpGet($url);

            $result['status'] = "Success";
            $result['msg'] = "Product List";
            $result['response'] = json_decode($product_list, true);
            return $result;
        } catch (Exception $ex) {
            $result['status'] = "Fail";
            $result['errors'] = $ex->getMessage();
            return $result;
        }
    }

    public static function httpGet($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    Public function searching($filter) {
        $CI = & get_instance();
        $CI->load->config('custom-config');
        $searchParam = $CI->config->item('SOLR_SEARCH_PARAM');
        $arr = array_keys($searchParam);

        $paramNotFound = array();
        $query_condition = '';
        foreach ($filter as $key => $value) {
            if (!in_array($key, $arr)) {
                $paramNotFound[] = $key;
                exit;
            } else {
                if ($searchParam[$key] == 'int') {
                    if (!empty($query_condition)) {
                        $query_condition.= " AND ";
                    }
                    if (is_array($value)) {
                        $value=array_map('trim', $value);
                        $search_param = implode(",", $value);
                        $query_condition.= "(" . $key . " : " . str_replace(",", " OR " . $key . " : ", $search_param) . ") ";
                    } else {
                        $query_condition.= "(" . $key . " : " . trim($value) . ") ";
                    }
                } else {
                    if (!empty($key)) {
                        if (!empty($query_condition)) {
                            $query_condition.= " AND ";
                        }
                        $query_condition.= "(" . $key . " : *" . trim($value) . "*) ";
                    }
                }
            }
        }
        if (!empty($paramNotFound)) {
                $result['status'] = 0;;
                $result['msg'] = "Invalid Searching Parameter";
            } else {
                $result['status'] = 1;
                $result['query_condition'] = $query_condition;
            }
        return $result;
    }

    
    Public function facets() {
        $CI = & get_instance();
        $CI->load->config('custom-config');
        $facetParam = $CI->config->item('SOLR_FACETS_PARAM');
        
       $facet_condition = '';
        foreach ($facetParam as $key => $value) {
            $facet_condition.='&facet.field='.$value;
        }
        return $facet_condition;
    }
    
    Public function sorting($sortingParam) {
        $sort_condition = '';
        foreach ($sortingParam as $key => $value) {
             $sort_condition.=$key." ". $value.",";
        }
        $sort_condition = rtrim($sort_condition, ',');
        return $sort_condition;
    }
}
