<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Solr URL 
//$config['SOLR_URL'] = 'http://localhost:8983/solr/collection1/';
$config['SOLR_URL'] = 'http://139.162.24.97:8983/solr/collection1/';

//Solr Searching Parameter
$config['SOLR_SEARCH_PARAM'] = array(
    'subscribed_product_id'=>'int',
    'base_product_id'=>'int',
    'campaign_id'=>'int',
    'store_id'=>'int',
    'customer_value'=>'int',
    'store_price'=>'int',
    'store_offer_price'=>'int',
    'dis_per'=>'int',
    'is_cod'=>'int',
    'weight'=>'int',
    'length'=>'int',
    'width'=>'int',
    'height'=>'int',
    'warranty'=>'string',
    'sku'=>'string',
    'product_content_type'=>'string',
    'ISBN'=>'string',
    'is_available'=>'int',
    'quantity'=>'int',
    'is_serial_required'=>'int',
    'title'=>'string',
    'small_description'=>'string',
    'description'=>'string',
    'color'=>'string',
    'size'=>'string',
    'product_weight'=>'int',
    'brand'=>'string',
    'model_name'=>'string',
    'model_number'=>'string',
    'manufacture'=>'string',
    'manufacture_country'=>'string',
    'key_features'=>'string',
    'average_rating'=>'int',
    'configurable_with'=>'string',
    'store_code'=>'string',
    'store_name'=>'string',
    'store_details'=>'string',
    'seller_name'=>'string',
    'business_address'=>'string',
    'business_address_country'=>'string',
    'business_address_state'=>'string',
    'business_address_city'=>'string',
    'business_address_pincode'=>'string',
    'visible'=>'int',
    'specifications'=>'string',
    'categories'=>'int',
    'category_paths'=>'string',
    'mega_categories'=>'int',
    'store_front_id'=>'int'
    );

$config['SOLR_MIN_PARAM'] = "subscribed_product_id,base_product_id,campaign_id,store_id,customer_value,store_price,store_offer_price,video_url,dis_per,is_cod,weight,length,width,height,warranty,sku,product_content_type,ISBN,prompt,prompt_key,created_at,modified_at";

$config['SOLR_MICRO_PARAM'] = "subscribed_product_id,base_product_id,campaign_id,store_id,customer_value,store_price,store_offer_price,video_url,dis_per,is_cod,weight,length,width,height,warranty,sku,product_content_type,ISBN,prompt,prompt_key,created_at,modified_at";

$config['SOLR_FACETS_PARAM'] = array("color","brand");