<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function index() {
        //return "hi";
    }

    public function productList() {
        $value= array();
        if (isset($_REQUEST)) {
            $value = $_REQUEST;
        }

        $this->load->library('product');
        $result = $this->product->productList($value);
        $json_data =  json_encode($result);
        $data['response'] = $json_data;             
        $this->load->view('responseData', $data);
    }

}
