<?PHP
define('DBUSER', 'root'); //change this value to your DB username
define('DBPASS', ''); //change this value to your DB password
define('DBHOST', 'localhost');
define('DBSCHEMA', 'solrtut');
define('SOLRNAME', '/solr/collection1/');
define('SOLRPORT', '8983');
define('SOLRHOST', 'localhost');

define('LIMIT', 10);
define('SOLRURL', 'http://localhost:8983/solr/');


define('BASEMEDIAURL', 'http://localhost/');
define('PRODUCTPATH', 'product/image/');
define('BASEURL', 'http://localhost/');
define('STOREFRONT_CHECKOUT_URL', 'http://localhost/');
define('STOREFRONT_PDP_URL', 'http://localhost/');
?>