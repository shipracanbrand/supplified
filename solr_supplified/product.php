<?php

require_once('functions.php');
require_once('constants.php');
require_once('solr.php');

try {
    echo "\nSolr reindex log script started : " . date("Y-m-d H:i:s");
    $allData = getALLProductList();
    $documents = array();
    $deletedAllIds = '';
    $updatedData = $deletedData = array();
    if ($allData) {
        foreach ($allData as $key => $value) {
            $deletedAllIds .= $value['subscribed_product_id'] . ",";
            if ($value['is_deleted'] == 0 && $value['subscribed_product_status'] == 1 && $value['base_product_status'] == 1 && $value['store_status'] == 1 && $value['quantity'] > 0) {

                //This is using for add categories and mega categories related to subscribed product
                $categoryData = getCategoryByBaseProductId($value['base_product_id']);
                $categories = array();
                $megaCategories = array();
                if ($categoryData) {
                    foreach ($categoryData as $category) {
                        if ($category['is_mega_category'] == 1) {
                            $megaCategories[] = $category['category_id'];
                        } else {
                            $categories[] = $category['category_id'];
                        }
                    }
                }
                if ($megaCategories)
                    $allData[$key]['mega_categories'] = $megaCategories;

                if ($categories) {
                    $allData[$key]['categories'] = $categories;
                    $allData[$key]['category_paths'] = getCategoryPathData($categories);
                }


                //Add base product media url
                $mediaData = getProductImageByProductId($value['base_product_id']);
                if ($mediaData) {
                    foreach ($mediaData as $media) {
                        $mediaUrl = $media['media_url'];
                        $thumbUrl = $media['thumb_url'];
                        /* if ($media['is_default'] == 1) {
                          $mediaUrl .= '|1';
                          $thumbUrl .= '|1';
                          } */
                        $allData[$key]['media_url'][] = BASEMEDIAURL . PRODUCTPATH . $mediaUrl;
                        $allData[$key]['thumb_url'][] = BASEMEDIAURL . PRODUCTPATH . $thumbUrl;
                    }
                }
                $allData[$key]['buy_now_url'] = BASEURL . 'checkout_process.php?product_id=' . $value['subscribed_product_id'] . '&payDetail=1&auto_req=1&auto_payment=1';

                if (isset($allData[$key]['thumb_url']['0']) AND ! empty($allData[$key]['thumb_url']['0'])) {
                    $allData[$key]['default_thumb_url'] = $allData[$key]['thumb_url']['0'];
                }

                //Store front checkout url
                $allData[$key]['checkout_url'] = STOREFRONT_CHECKOUT_URL . $value['subscribed_product_id'];

                //Store front pdp url
                $allData[$key]['pdp_url'] = STOREFRONT_PDP_URL . $value['subscribed_product_id'];

                //Add store front ids
                $storefrontData = getStoreDetailByProductId($value['subscribed_product_id']);

                $storefronts = array();
                if (!empty($storefrontData)) {
                    foreach ($storefrontData as $storefront) {
                        $storefronts[] = $storefront['store_front_id'];
                    }
                }
                if (!empty($storefronts))
                    $allData[$key]['store_front_id'] = $storefronts;

                if (!empty($value['configurable_with'])) {
                    $allData[$key]['configurable_with'] = $color = $size = null;
                    if (!empty($value['color'])) {
                        $color = getColorProductByProductId($value['store_id'], $value['configurable_with']);
                    }

                    if (!empty($value['size'])) {
                        $size = getSizeProductByProductId($value['store_id'], $value['configurable_with']);
                    }

                    if (!empty($color) OR ! empty($size)) {
                        $allData[$key]['configurable_with'] = json_encode(array('color' => $color, 'size' => $size));
                    }
                }

                $updatedData[] = $value['subscribed_product_id'];
            } else {
                $deletedData[] = $value['subscribed_product_id'];
            }
        }
        $solr = new SOLR();
        if ($allData) {
            echo "\nUpdated subscribed product Ids : " . implode("\n", $updatedData);
            //$solr->deleteDocs($allData);
            $solr->saveEntityIndexes($allData);
        }
        if ($deletedData) {
            echo "\nDeleted subscribed product Ids : " . implode("\n", $deletedData);
            $solr->deleteDocs($deletedData);
        }
        if ($allData) {
            $arr = array();
           // $queryDeleteIds = DeleteSolrBackLog($deletedAllIds);
            //echo $queryDeleteIds;

          
        }
    }
    echo "\nSolr reindex log script ended : " . date("Y-m-d H:i:s");
} catch (Exception $e) {
    echo $e->getMessage();
}
?>