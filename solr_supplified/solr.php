<?php

require_once('Apache/Solr/Document.php');
require_once('Apache/Solr/Service.php');

class SOLR {

    public function saveEntityIndexes($entityIndexes) {
        $solrObj = new Apache_Solr_Service(SOLRHOST, SOLRPORT, SOLRNAME);
       //print_r($solrObj);
//        if (!$solrObj->ping()) {
//            echo 'Solr service not responding for user Detail.';
//            exit;
//        }
      
        $documents = array();
        $arrCategories = array();
        $arrBaseProductids = array();

        foreach ($entityIndexes as $key => $index) {
          
            if ($index['base_product_id']) {
                $index['small_desciption'] = (isset($index['small_desciption'])) ? $index['small_desciption'] : '';
            }
            $index['description'] = (isset($index['description'])) ? $index['description'] : '';
            $index['store_price'] = (isset($index['store_price'])) ? $index['store_price'] : 0;

            $metaData = (isset($index['meta_keyword'])) ? $index['meta_keyword'] : '';
            $metaData .= (isset($index['meta_title'])) ? ' ' . $index['meta_title'] : '';
            $metaData .= (isset($index['meta_description'])) ? ' ' . $index['meta_description'] : '';
            $fulltext = $index['title'] . ' ' . $index['store_name'] . ' ' .
                    $index['small_description'] . ' ' . $index['description'] . ' ' . $metaData;

            $document = new Apache_Solr_Document();

            $document->subscribed_product_id = (int) $index['subscribed_product_id'];
            try{
                $solrObj->deleteById($index['subscribed_product_id']);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }  
            $document->base_product_id = $index['base_product_id'];
            $document->campaign_id = $index['campaign_id'];
            $document->store_id = $index['store_id'];
            $document->customer_value = (isset($index['customer_value'])) ? $index['customer_value'] : 0;
            $document->store_price = $index['store_price'];
            $document->store_offer_price = (isset($index['store_offer_price'])) ? $index['store_offer_price'] : 0;

            $getStoreprice = $document->store_price;
            $getStoreofferprice = $document->store_offer_price;
            $differAmount = ($getStoreprice - $getStoreofferprice);
            $per = ($differAmount / $getStoreprice);
            $final_pecent = ($per * 100);

            $document->video_url = (isset($index['video_url'])) ? $index['video_url'] : '';

            $document->dis_per = floor($final_pecent);
            $document->is_cod = (isset($index['is_cod'])) ? $index['is_cod'] : 0;
            $document->weight = (isset($index['weight'])) ? $index['weight'] : 0;

            $document->length = (isset($index['length'])) ? $index['length'] : 0;
            $document->width = (isset($index['width'])) ? $index['width'] : 0;
            $document->height = (isset($index['height'])) ? $index['height'] : 0;
            $document->warranty = (isset($index['warranty'])) ? $index['warranty'] : '';

            $document->sku = (isset($index['sku'])) ? $index['sku'] : '';
            $document->product_content_type = (isset($index['product_content_type'])) ? $index['product_content_type'] : '';
            $document->ISBN = (isset($index['ISBN'])) ? $index['ISBN'] : '';

            if (isset($index['prompt'])) {
                $document->prompt = $index['prompt'];
            }
            if (isset($index['prompt_key'])) {
                $document->prompt_key = $index['prompt_key'];
            }
            $document->created_at = gmdate('Y-m-d\TH:i:s\Z', strtotime($index['created_date']));
            $document->modified_at = gmdate('Y-m-d\TH:i:s\Z', strtotime($index['modified_date']));
            $document->is_available = ($index['quantity'] > 0 ? '1' : '0');
            $document->quantity = $index['quantity'];
            $document->is_serial_required = $index['is_serial_required'];
            $document->store_create_date = gmdate('Y-m-d\TH:i:s\Z', strtotime($index['store_create_date']));
            $document->title = (isset($index['title'])) ? utf8_encode(trim($index['title'])) : '';
            $document->small_description = $index['small_description'];

            /* fix for json blank value: encoding for description */
            $description = utf8_encode($index['description']);
            $document->description = (isset($index['description'])) ? $description : '';
            $document->color = (isset($index['color'])) ? $index['color'] : '';
            $document->size = (isset($index['size'])) ? $index['size'] : '';
            $document->product_weight = (isset($index['product_weight'])) ? $index['product_weight'] : 0;
            $document->brand = (isset($index['brand'])) ? $index['brand'] : '';
            $document->model_name = (isset($index['model_name'])) ? $index['model_name'] : '';
            $document->model_number = (isset($index['model_number'])) ? $index['model_number'] : '';
            $document->manufacture = (isset($index['manufacture'])) ? $index['manufacture'] : '';
            $document->manufacture_country = (isset($index['manufacture_country'])) ? $index['manufacture_country'] : '';

            if (isset($index['manufacture_year'])) {
                $document->manufacture_year = $index['manufacture_year'];
            }
            $document->key_features = (isset($index['key_features'])) ? $index['key_features'] : '';
            $document->average_rating = (isset($index['average_rating'])) ? $index['average_rating'] : 0;
            $document->is_configurable = $index['is_configurable'] ? true : false;
            $document->configurable_with = (isset($index['configurable_with'])) ? $index['configurable_with'] : '';
            $document->store_code = (isset($index['store_code'])) ? $index['store_code'] : '';
            $document->store_name = (isset($index['store_name'])) ? $index['store_name'] : '';
            $document->email = (isset($index['email'])) ? $index['email'] : '';
            $document->store_details = (isset($index['store_details'])) ? $index['store_details'] : '';
            $document->store_logo = !empty($index['store_logo']) ? BASEMEDIAURL . $index['store_logo'] : '';
            $document->seller_name = (isset($index['seller_name'])) ? $index['seller_name'] : '';
            $document->business_address = (isset($index['business_address'])) ? $index['business_address'] : '';

            /* fix for separate fields for business address country,state,city and pincode */
            $document->business_address_country = (isset($index['business_address_country'])) ? $index['business_address_country'] : '';
            $document->business_address_state = (isset($index['business_address_state'])) ? $index['business_address_state'] : '';
            $document->business_address_city = (isset($index['business_address_city'])) ? $index['business_address_city'] : '';
            $document->business_address_pincode = (isset($index['business_address_pincode'])) ? $index['business_address_pincode'] : '';
            $document->mobile_numbers = (isset($index['mobile_numbers'])) ? $index['mobile_numbers'] : '';
            $document->telephone_numbers = (isset($index['telephone_numbers'])) ? $index['telephone_numbers'] : '';
            $document->visible = $index['visible'] ? true : false;
            $document->categories = (isset($index['categories'])) ? $index['categories'] : 0;
            $document->category_paths = (isset($index['category_paths'])) ? $index['category_paths'] : 0;
            $document->mega_categories = (isset($index['mega_categories'])) ? $index['mega_categories'] : 0;
            $document->store_front_id = (isset($index['store_front_id'])) ? $index['store_front_id'] : 0;
            $document->media_url = (isset($index['media_url'])) ? $index['media_url'] : array();
            $document->thumb_url = (isset($index['thumb_url'])) ? $index['thumb_url'] : array();
            $document->buy_now_url = (isset($index['buy_now_url'])) ? $index['buy_now_url'] : '';
            $document->checkout_url = (isset($index['checkout_url'])) ? $index['checkout_url'] : '';
            $document->pdp_url = (isset($index['pdp_url'])) ? $index['pdp_url'] : '';
            $document->default_thumb_url = (isset($index['default_thumb_url'])) ? $index['default_thumb_url'] : '';
            $document->specifications = (isset($index['specifications'])) ? $index['specifications'] : '';
            $document->fulltext = utf8_encode($fulltext);
            $documents[] = $document;
        }
        try {
          // print_r($documents);
            $solrObj->addDocuments($documents);
            $solrObj->commit();
            $solrObj->optimize();
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
//        try {
//            $service = new Apache_Solr_Service('');
//            //900000ms = 15 mins
//             $service->addDocuments($documents, false, true, true, 900000);
//            //we need to clean cache after reindex so sending commit commend to solr instead of commit within 
//            $service->addDocuments($documents, false, true, true, 0);
//            $service->commit();
//            $service->optimize();
//            //cleans cache
//            //$this->cleanCache($virtualstoreid, $storecode, $arrCategories, $arrBaseProductids);
//        } catch (Exception $e) {
//            $this->rollback();
//            echo $e->getMessage();
//            //Mage::logException($e);
//            //Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('catalogsearch')->__('Can not index data in Solr. %s',$e->getMessage()));
//        }
//        return $this;
    }

    /**
     * Rollbacks all add/deletes made to the index since the last commit
     *
     * @return object
     */
    public function rollback() {
        $service = new Apache_Solr_Service();
        return $service->rollback();
    }

    /**
     * Remove documents from Solr index
     *
     * @param int|string|array $docIDs
     * @param string|array $queries if "all" specified and $docIDs are empty, then all documents will be removed
     * @return unknown
     */
    public function deleteDocs($docIDs = array(), $queries = null) {
        $_deleteBySuffix = 'MultipleIds';
        $params = array();
        $solrObj = new Apache_Solr_Service(SOLRHOST, SOLRPORT, SOLRNAME);
        if (!empty($docIDs)) {
            if (!is_array($docIDs)) {
                $docIDs = array($docIDs);
            }
            $params = $docIDs;
        } elseif (!empty($queries)) {
            if ($queries == 'all') {
                $queries = array('*:*');
            }
            if (!is_array($queries)) {
                $queries = array($queries);
            }
            $_deleteBySuffix = 'Queries';
            $params = $queries;
        }
        if ($params) {
            $deleteMethod = sprintf('deleteBy%s', $_deleteBySuffix);
            try {
                $response=$solrObj->$deleteMethod($params);
                $solrObj->commit();
                $solrObj->optimize();
            } catch (Exception $ex) {
                //echo $ex->getMessage();
            }
//            try {
//                $service = new Apache_Solr_Service();
//                $response = $service->$deleteMethod($params);
//                $service->commit();
//            } catch (Exception $e) {
//                $this->rollback();
//            }
        }

        return $this;
    }

}
