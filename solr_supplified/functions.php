<?php

require_once('connection.php');

function getALLProductList() {
    $Arr = array();
    $sql = "SELECT sp.subscribed_product_id, sp.getit_subscribed_product_id, sp.base_product_id, sp.store_id, sp.store_price, sp.store_offer_price, sp.weight, sp.length, sp.width, sp.height, sp.warranty, sp.prompt, sp.prompt_key, sp.status as subscribed_product_status, sp.created_date, sp.modified_date, sp.quantity, sp.is_cod, sp.sku, bp.product_content_type, bp.ISBN, bp.base_product_id, bp.getit_base_product_id, bp.title, bp.small_description, bp.description, bp.color, bp.size, bp.product_weight, bp.brand, bp.model_name, bp.model_number, bp.manufacture, bp.manufacture_country, bp.manufacture_year, bp.key_features, bp.campaign_id, bp.average_rating, bp.is_configurable, bp.configurable_with, bp.status as base_product_status, bp.specifications, bp.video_url, st.store_id, st.getit_store_id, st.store_code, st.store_name, st.store_details, st.store_logo, st.seller_name, st.email, st.business_address, st.business_address_country, st.business_address_state, st.business_address_city, st.business_address_pincode, st.mobile_numbers, st.telephone_numbers, st.customer_value, st.visible, st.created_date as store_create_date, bp.meta_title, bp.meta_keyword, bp.meta_description, bp.is_serial_required, st.vtiger_accountid as vtiger_accountid, st.status as store_status, st.vtiger_status as vtiger_store_status, sbl.is_deleted FROM subscribed_product sp INNER JOIN solr_back_log sbl ON sbl.subscribed_product_id = sp.subscribed_product_id INNER JOIN base_product bp ON bp.base_product_id = sp.base_product_id INNER JOIN store st ON st.store_id = sp.store_id LIMIT 0, 4000";
    $result = mysql_query($sql);
    $count = mysql_num_rows($result);
    if ($count) {
        while ($dataArr = mysql_fetch_array($result)) {
            $Arr[] = $dataArr;
        }
        return $Arr;
    } else {
        return $Arr;
    }
}

function getCategoryByBaseProductId($id) {
    
    $Arr = array();
    $sql = "SELECT pcm.base_product_id,pcm.category_id,c.is_mega_category FROM product_category_mapping pcm INNER JOIN category c ON pcm.category_id = c.category_id WHERE pcm.base_product_id=$id";
    $result = mysql_query($sql);
    $count = mysql_num_rows($result);
    if ($count) {
        while ($dataArr = mysql_fetch_array($result)) {
            $Arr[] = $dataArr;
        }
        return $Arr;
    } else {
        return $Arr;
    }
}

function getStoreDetailByProductId($id) {
    $Arr = array();
    $sql = "SELECT store_front_id FROM product_frontend_mapping WHERE subscribed_product_id=" . $id;
    $result = mysql_query($sql);
    $count = mysql_num_rows($result);
    if ($count) {
        while ($dataArr = mysql_fetch_array($result)) {
            $Arr[] = $dataArr;
        }
        return $Arr;
    } else {
        return $Arr;
    }
}

function getProductImageByProductId($id) {
    $Arr = array();
    $sql = "select media_url, thumb_url, is_default from media where base_product_id=" . $id . " ORDER BY is_default DESC";
    $result = mysql_query($sql);
    $count = mysql_num_rows($result);
    if ($count) {
        while ($dataArr = mysql_fetch_array($result)) {
            $Arr[] = $dataArr;
        }
        return $Arr;
    } else {
        return $Arr;
    }
}

function getColorProductByProductId($store_id, $configurable_with) {
    $Arr = array();
    $sql = "SELECT sp.subscribed_product_id, sp.base_product_id, bp.color AS value FROM subscribed_product AS sp LEFT JOIN base_product AS bp ON sp.base_product_id = bp.base_product_id WHERE sp.store_id = " . $store_id . " AND sp.is_deleted = 0 AND sp.status = 1 AND bp.is_deleted = 0 AND bp.status = 1 AND bp.base_product_id IN (" . $configurable_with . ") AND bp.color != ''";
    $result = mysql_query($sql);
    $count = mysql_num_rows($result);
    if ($count) {
        while ($dataArr = mysql_fetch_array($result)) {
            $Arr[] = $dataArr;
        }
        return $Arr;
    } else {
        return $Arr;
    }
}

function getSizeProductByProductId($store_id, $configurable_with) {
    $Arr = array();
    $sql = "SELECT sp.subscribed_product_id, sp.base_product_id, bp.size AS value FROM subscribed_product AS sp LEFT JOIN base_product AS bp ON sp.base_product_id = bp.base_product_id WHERE sp.store_id = " . $store_id . " AND sp.is_deleted = 0 AND sp.status = 1 AND bp.is_deleted = 0 AND bp.status = 1 AND bp.base_product_id IN (" . $configurable_with . ") AND bp.size != ''";
    $result = mysql_query($sql);
    $count = mysql_num_rows($result);
    if ($count) {
        while ($dataArr = mysql_fetch_array($result)) {
            $Arr[] = $dataArr;
        }
        return $Arr;
    } else {
        return $Arr;
    }
}

function DeleteSolrBackLog($deletedAllIds) {
    $sql = "delete from solr_back_log where subscribed_product_id in (" . trim($deletedAllIds, ",") . ")";
    $result = mysql_query($sql);
    if (!$result) {
        return('Could not delete data: ' . mysql_error());
    }
    return "Deleted data successfully\n";
}

/**
 * 
 * Fetch category paths from category ids 
 * @param array $categoryIds
 */
function getCategoryPathData($categoryIds = null) {
    if (!is_array($categoryIds))
        return false;
    $categoryIds = array(max($categoryIds));
    //$mysql = new MYSQL();
    $categoryPathData = $categoryData = array();
    foreach ($categoryIds as $categoryId) {
        $sql = "SELECT * from category where category_id = $categoryId";
        $category = mysql_query($sql);
        switch ($category['level']) {
            case 2:
                $categoryData[] = $category['category_id'] . ':' . $category['category_name'];
                break;
            case 3:
                $sql = "SELECT * from category where category_id = " . $category['parent_category_id'];
                $l1cat = mysql_query($sql);
                $categoryData[] = $l1cat['category_id'] . ':' . $l1cat['category_name'];
                $categoryData[] = $category['category_id'] . ':' . $category['category_name'];
                break;
            case 4:
                $sql = "SELECT * from category where category_id = " . $category['parent_category_id'];
                $l2cat = mysql_query($sql);
                $sql = "SELECT * from category where category_id = " . $l2cat['parent_category_id'];
                $l1cat = $mysql->fetch($sql);
                $categoryData[] = $l1cat['category_id'] . ':' . $l1cat['category_name'];
                $categoryData[] = $l2cat['category_id'] . ':' . $l2cat['category_name'];
                $categoryData[] = $category['category_id'] . ':' . $category['category_name'];
                break;
            case 5:
                $sql = "SELECT * from category where category_id = " . $category['parent_category_id'];
                $l3cat = mysql_query($sql);
                $sql = "SELECT * from category where category_id = " . $l3cat['parent_category_id'];
                $l2cat = mysql_query($sql);
                $sql = "SELECT * from category where category_id = " . $l2cat['parent_category_id'];
                $l1cat = mysql_query($sql);
                $categoryData[] = $l1cat['category_id'] . ':' . $l1cat['category_name'];
                $categoryData[] = $l2cat['category_id'] . ':' . $l2cat['category_name'];
                $categoryData[] = $l3cat['category_id'] . ':' . $l3cat['category_name'];
                $categoryData[] = $category['category_id'] . ':' . $category['category_name'];
                break;
            case 6:
                $sql = "SELECT * from category where category_id = " . $category['parent_category_id'];
                $l4cat = mysql_query($sql);
                $sql = "SELECT * from category where category_id = " . $l4cat['parent_category_id'];
                $l3cat = mysql_query($sql);
                $sql = "SELECT * from category where category_id = " . $l3cat['parent_category_id'];
                $l2cat = mysql_query($sql);
                $sql = "SELECT * from category where category_id = " . $l2cat['parent_category_id'];
                $l1cat = $mysql->fetch($sql);
                $categoryData[] = $l1cat['category_id'] . ':' . $l1cat['category_name'];
                $categoryData[] = $l2cat['category_id'] . ':' . $l2cat['category_name'];
                $categoryData[] = $l3cat['category_id'] . ':' . $l3cat['category_name'];
                $categoryData[] = $l4cat['category_id'] . ':' . $l4cat['category_name'];
                $categoryData[] = $category['category_id'] . ':' . $category['category_name'];
                break;
            default:
                break;
        }
        $categoryPathData[] = implode('|', $categoryData);
    }
    return array_values(array_unique($categoryPathData));
}

?>